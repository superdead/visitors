FROM python:3.9-alpine

RUN mkdir /app
COPY requirements.txt /app
WORKDIR /app 

RUN pip3 install -r requirements.txt

COPY . /app

CMD python app.py

EXPOSE 8080